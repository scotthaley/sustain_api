defmodule SustainApi.Item do
  use Ecto.Schema
  import Ecto.Changeset
  alias SustainApiWeb.Auth
  alias SustainApi.User

  schema "items" do
    field(:name, :string)
    field(:default_measurement, :string)
    field(:description, :string)
    belongs_to(:user, User)

    timestamps()
  end

  @doc false
  def changeset(item, attrs) do
    item
    |> cast(attrs, [:name, :user_id, :default_measurement, :description])
    |> validate_required([:name, :default_measurement])
    |> unique_constraint(:existing_item, name: :item_name_unique)
    |> assoc_constraint(:user)
  end
end
