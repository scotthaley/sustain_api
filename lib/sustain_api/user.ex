defmodule SustainApi.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias SustainApi.{Repo, User, UserHousehold, Item, Recipe}

  def by_id(id), do: Repo.get(User, id)

  schema "users" do
    field(:email, :string)
    field(:password_digest, :string)
    field(:current_household, :integer)
    has_many(:user_households, UserHousehold)
    has_many(:households, through: [:user_households, :household])
    has_many(:items, Item)
    has_many(:recipes, Recipe)

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :password_digest, :current_household])
    |> validate_required([:email, :password_digest])
    |> unique_constraint(:existing_account, name: :email_unique)
  end
end
