defmodule SustainApi.Recipe do
  use Ecto.Schema
  import Ecto.Changeset
  alias SustainApi.User

  schema "recipes" do
    field :name, :string
    belongs_to(:user, User)

    timestamps()
  end

  @doc false
  def changeset(recipe, attrs) do
    recipe
    |> cast(attrs, [:name])
    |> validate_required([:name])
    |> assoc_constraint(:user)
  end
end
