defmodule SustainApi.Test do
  use Ecto.Schema
  import Ecto.Changeset

  schema "tests" do
    field :hello, :string
    field :whatisthis, :string

    timestamps()
  end

  @doc false
  def changeset(test, attrs) do
    test
    |> cast(attrs, [:hello, :whatisthis])
    |> validate_required([:hello, :whatisthis])
  end
end
