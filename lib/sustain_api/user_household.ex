defmodule SustainApi.UserHousehold do
  use Ecto.Schema
  import Ecto.Changeset
  alias SustainApi.{User, Household}

  schema "user_households" do
    belongs_to :user, User
    belongs_to :household, Household

    timestamps()
  end

  @doc false
  def changeset(user_household, attrs) do
    user_household
    |> cast(attrs, [:user_id, :household_id])
    |> validate_required([])
    |> foreign_key_constraint(:user_id)
    |> foreign_key_constraint(:household_id)
  end
end
