defmodule SustainApi.Household do
  use Ecto.Schema
  import Ecto.Changeset
  alias SustainApi.{User}

  schema "households" do
    field :name, :string
    belongs_to(:owner, User, foreign_key: :user_id)

    timestamps()
  end

  @doc false
  def changeset(household, attrs) do
    household
    |> cast(attrs, [:name, :user_id])
    |> validate_required([:name])
    |> assoc_constraint(:owner)
  end
end
