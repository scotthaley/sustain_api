defmodule SustainApi.Repo do
  use Ecto.Repo,
    otp_app: :sustain_api,
    adapter: Ecto.Adapters.Postgres
end
