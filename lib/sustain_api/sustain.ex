defmodule SustainApi.Sustain do
  @moduledoc """
  The boundry for the Sustain API
  """

  import Ecto.{Changeset}
  alias SustainApi.{Repo, Household, UserHousehold, Item, Recipe}

  @doc """
  Get a resource of the given type and id. If a user is provided,
  check that the user owns the requested resource before returning.

  ## Examples

    iex> get_resource(ResourceType, id)
    {:ok, %ResourceType{}}

    iex> get_resource(ResourceType, id, %User{})
    {:error, :unauthorized}

  """
  def get_resource(resource, id) do
    case r = Repo.get(resource, id) do
      %^resource{} -> {:ok, r}
      _ -> {:error, :notfound}
    end
  end

  def get_resource(resource, id, user) do
    with {:ok, r} <- get_resource(resource, id) do
      case r.user_id == user.id do
        true -> {:ok, r}
        _ -> {:error, :unauthorized}
      end
    end
  end

  @doc """
  Creates a household and associates it with the given user

  ## Examples

    iex> create_household(%{name: value})
    {:ok, %Household{}}

  """
  def create_household(attrs \\ %{}, user) do
    Repo.transaction(fn ->
      household =
        Household.changeset(%Household{}, attrs)
        |> put_assoc(:owner, user)
        |> Repo.insert!()

      user_household =
        UserHousehold.changeset(%UserHousehold{}, %{})
        |> put_assoc(:household, household)
        |> put_assoc(:user, user)
        |> Repo.insert!()

      household
    end)
  end

  def get_household(id), do: get_resource(Household, id)
  def get_household(id, user), do: get_resource(Household, id, user)

  @doc """
  Creates an item and associates it with the given user

  ## Examples

    iex> create_item(%{name: "celery", ...})
    {:ok, %Item{}}

  """
  def create_item(attrs \\ %{}, user) do
    Item.changeset(%Item{}, attrs)
    |> put_assoc(:user, user)
    |> Repo.insert()
  end

  def get_item(id), do: get_resource(Item, id)
  def get_item(id, user), do: get_resource(Item, id, user)

  @doc """
  Creates a recipe and associates it with the given user

  ## Examples

    iex> create_recipe(%{name: value})
    {:ok, %Recipe{}}

  """
  def create_recipe(attrs \\ %{}, user) do
    Recipe.changeset(%Recipe{}, attrs)
    |> put_assoc(:user, user)
    |> Repo.insert()
  end

  def get_recipe(id), do: get_resource(Recipe, id)
  def get_recipe(id, user), do: get_resource(Recipe, id, user)
end
