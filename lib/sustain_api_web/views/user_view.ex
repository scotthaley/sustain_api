defmodule SustainApiWeb.UserView do
  use SustainApiWeb, :view
  alias SustainApiWeb.UserView

  def render("show.json", %{user: user}) do
    %{user: render_one(user, UserView, "user.json")}
  end

  def render("user.json", %{user: user}) do
    user
    |> Map.from_struct()
    |> Map.take([:id, :email])
  end
end
