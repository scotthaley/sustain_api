defmodule SustainApiWeb.ItemView do
  use SustainApiWeb, :view
  alias SustainApiWeb.{ItemView, UserView}

  def render("index.json", %{items: items}) do
    %{
      items: render_many(items, ItemView, "item.json"),
      count: length(items)
    }
  end

  def render("show.json", %{item: item}) do
    %{item: render_one(item, ItemView, "item.json")}
  end

  def render("item.json", %{item: item}) do
    item
    |> Map.from_struct()
    |> Map.take([:id, :name, :description, :user])
    |> Map.put(:user, UserView.render("user.json", user: item.user))
  end
end
