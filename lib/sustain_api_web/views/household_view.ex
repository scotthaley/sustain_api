defmodule SustainApiWeb.HouseholdView do
  use SustainApiWeb, :view
  alias SustainApiWeb.{HouseholdView, UserView}

  def render("index.json", %{households: households}) do
    %{
      households: render_many(households, HouseholdView, "household.json"),
      count: length(households)
    }
  end

  def render("show.json", %{household: household}) do
    %{household: render_one(household, HouseholdView, "household.json")}
  end

  def render("household.json", %{household: household}) do
    household
    |> Map.from_struct()
    |> Map.take([:id, :name, :owner])
    |> Map.put(:owner, UserView.render("user.json", user: household.owner))
  end
end
