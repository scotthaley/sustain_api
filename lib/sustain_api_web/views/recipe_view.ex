defmodule SustainApiWeb.RecipeView do
  use SustainApiWeb, :view
  alias SustainApiWeb.{RecipeView, UserView}

  def render("index.json", %{recipes: recipes}) do
    %{
      recipes: render_many(recipes, RecipeView, "recipe.json"),
      count: length(recipes)
    }
  end

  def render("show.json", %{recipe: recipe}) do
    %{recipe: render_one(recipe, RecipeView, "recipe.json")}
  end

  def render("recipe.json", %{recipe: recipe}) do
    recipe
    |> Map.from_struct()
    |> Map.take([:id, :name, :user])
    |> Map.put(:user, UserView.render("user.json", user: recipe.user))
  end
end
