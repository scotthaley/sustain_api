defmodule SustainApiWeb.Router do
  use SustainApiWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  pipeline :api_auth do
    plug(SustainApiWeb.Guardian.AuthPipeline)
  end

  scope "/", SustainApiWeb do
    pipe_through(:browser)

    get("/", PageController, :index)
  end

  scope "/", SustainApiWeb do
    pipe_through(:api)

    post("/user/signup", UserController, :signup)
    post("/user/signin", UserController, :sign_in)
    resources("/user", UserController, only: [:show])
  end

  scope "/", SustainApiWeb do
    pipe_through([:api, :api_auth])

    get("/profile", UserController, :profile)

    resources("/items", ItemController, except: [:edit, :new])
    resources("/households", HouseholdController, except: [:edit, :new])
    resources("/recipes", RecipeController, except: [:edit, :new])
  end
end
