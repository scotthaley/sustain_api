defmodule SustainApiWeb.ItemController do
  use SustainApiWeb, :controller
  use SustainApiWeb.GuardedController

  alias SustainApi.{Sustain, Repo, Item, User}

  action_fallback(SustainApiWeb.FallbackController)

  plug(
    Guardian.Plug.EnsureAuthenticated
    when action in [
           :index,
           :create,
           :update,
           :delete
         ]
  )

  def index(conn, _params, user) do
    user = user |> Repo.preload([:items])
    render(conn, "index.json", %{items: user.items})
  end

  def create(conn, params, user) do
    with {:ok, %Item{} = item} <- Sustain.create_item(params, user) do
      conn
      |> put_status(:created)
      |> render("show.json", %{item: item})
    end
  end

  def delete(conn, %{"id" => id}, user) do
    with {:ok, item} <- Sustain.get_item(id, user) do
      case Repo.delete(item) do
        {:ok, _struct} -> json(conn, %{success: true})
        _ -> json(conn, %{success: false})
      end
    end
  end

  def show(conn, %{"id" => id}, _user) do
    with {:ok, item} <- Sustain.get_item(id) do
      item =
        item
        |> Repo.preload([:user])

      render(conn, "show.json", %{item: item})
    end
  end

  def update(conn, params = %{"id" => id}, user) do
    with {:ok, item} <- Sustain.get_item(id, user) do
      changeset = Item.changeset(item, params)

      case Repo.update(changeset) do
        {:ok, item} ->
          render(conn, "show.json", %{item: item})
      end
    end
  end
end
