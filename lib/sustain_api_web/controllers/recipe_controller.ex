defmodule SustainApiWeb.RecipeController do
  use SustainApiWeb, :controller
  use SustainApiWeb.GuardedController

  alias SustainApi.{Sustain, Repo, Recipe}

  action_fallback(SustainApiWeb.FallbackController)

  plug(
    Guardian.Plug.EnsureAuthenticated
    when action in [
           :index,
           :create,
           :update,
           :delete
         ]
  )

  def index(conn, _params, user) do
    user = user |> Repo.preload([:recipes])
    render(conn, "index.json", %{recipes: user.recipes})
  end

  def create(conn, params, user) do
    with {:ok, %Recipe{} = recipe} <- Sustain.create_recipe(params, user) do
      conn
      |> put_status(:created)
      |> render("show.json", %{recipe: recipe})
    end
  end

  def delete(conn, %{"id" => id}, user) do
    with {:ok, recipe} <- Sustain.get_recipe(id, user) do
      case Repo.delete(recipe) do
        {:ok, _struct} -> json(conn, %{success: true})
        _ -> json(conn, %{success: false})
      end
    end
  end

  def show(conn, %{"id" => id}, _user) do
    with {:ok, recipe} <- Sustain.get_recipe(id) do
      recipe =
        recipe
        |> Repo.preload([:user])

      render(conn, "show.json", %{recipe: recipe})
    end
  end

  def update(conn, params = %{"id" => id}, user) do
    with {:ok, recipe} <- Sustain.get_recipe(id, user) do
      changeset = Recipe.changeset(recipe, params)

      case Repo.update(changeset) do
        {:ok, recipe} ->
          render(conn, "show.json", %{recipe: recipe})
      end
    end
  end
end
