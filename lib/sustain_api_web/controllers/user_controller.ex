defmodule SustainApiWeb.UserController do
  use SustainApiWeb, :controller
  use SustainApiWeb.GuardedController

  alias SustainApiWeb.Auth
  alias SustainApi.{Repo, User}

  action_fallback(SustainApiWeb.FallbackController)

  plug(
    Guardian.Plug.EnsureAuthenticated
    when action in [
           :profile
         ]
  )

  def profile(conn, _params, user) do
    render(conn, "show.json", %{user: user})
  end

  def show(conn, %{"id" => id}, _user) do
    with %User{} = user <- User.by_id(id) do
      render(conn, "show.json", %{user: user})
    end
  end

  @doc """
  User signup
  """
  def signup(conn, %{"email" => email, "password" => password}, _user) do
    changeset =
      User.changeset(
        %User{},
        %{email: email, password_digest: Auth.hash_password(password)}
      )

    case Repo.insert(changeset) do
      {:ok, user} ->
        json(conn, %{user: %{email: user.email}})

      {:error, changeset} ->
        json(conn, %{errors: changeset.errors |> Enum.map(&elem(&1, 0))})
    end
  end

  def sign_in(conn, %{"email" => email, "password" => password}, _user) do
    with %User{} = user <- Auth.get_resource_by_id(email) do
      with {:ok, token, _claims} <- Auth.authenticate(user, password) do
        json(conn, %{token: token})
      end
    end
  end
end
