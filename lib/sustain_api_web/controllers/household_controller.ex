defmodule SustainApiWeb.HouseholdController do
  use SustainApiWeb, :controller
  use SustainApiWeb.GuardedController

  alias SustainApi.{Sustain, Repo, Household, User}

  action_fallback(SustainApiWeb.FallbackController)

  plug(Guardian.Plug.EnsureAuthenticated)

  def index(conn, _params, user) do
    user = user |> Repo.preload([:households])
    render(conn, "index.json", %{households: user.households})
  end

  def create(conn, params, user) do
    with {:ok, %Household{} = household} <- Sustain.create_household(params, user) do
      conn
      |> put_status(:created)
      |> render("show.json", %{household: household})
    end
  end

  def delete(conn, %{"id" => id}, user) do
    with {:ok, household} <- Sustain.get_household(id, user) do
      case Repo.delete(household) do
        {:ok, _struct} -> json(conn, %{success: true})
        _ -> json(conn, %{success: false})
      end
    end
  end

  def show(conn, %{"id" => id}, user) do
    with {:ok, household} <- Sustain.get_household(id, user) do
      household =
        household
        |> Repo.preload([:owner])

      render(conn, "show.json", %{household: household})
    end
  end

  def update(conn, params = %{"id" => id}, user) do
    with {:ok, household} <- Sustain.get_household(id, user) do
      changeset = Household.changeset(household, params)

      case Repo.update(changeset) do
        {:ok, household} ->
          render(conn, "show.json", %{household: household})
      end
    end
  end
end
