defmodule SustainApiWeb.FallbackController do
  use SustainApiWeb, :controller
  require Logger

  def call(conn, nil) do
    json(conn, %{error: "not found"})
  end

  def call(conn, {:error, %Ecto.Changeset{errors: errors}}) do
    errors =
      errors
      |> Enum.map(fn e -> %{elem(e, 0) => elem(elem(e, 1), 0)} end)

    json(conn, %{errors: errors})
  end

  def call(conn, {:error, code}) do
    json(conn, %{error: code})
  end

  def call(conn, error) do
    json(conn, %{error: inspect(error)})
  end
end
