defmodule SustainApiWeb.PageController do
  use SustainApiWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
