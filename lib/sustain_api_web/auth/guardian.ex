defmodule SustainApiWeb.Guardian do
  use Guardian, otp_app: :sustain_api

  def subject_for_token(resource, _claims), do: {:ok, to_string(resource.email)}
  def subject_for_token(_, _), do: {:error, "Unknown resource type"}

  def resource_from_claims(%{"sub" => id}), do: {:ok, SustainApiWeb.Auth.get_resource_by_id(id)}
  def resource_from_claims(_claims), do: {:error, "Unknown resource type"}
end
