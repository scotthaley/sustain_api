defmodule SustainApiWeb.Auth do
  alias SustainApi.{Repo, User}
  alias Argon2
  import Ecto.Query, only: [from: 2]
  import Ecto.Changeset

  def get_resource_by_id(id) do
    user = Repo.one(from u in User, where: u.email == type(^id, :string))

    case user do
      nil -> {:error, :notfound}
      _ -> user
    end
  end

  def hash_password(password), do: Argon2.hash_pwd_salt(password)
  def hash_password(changeset), do: changeset

  def authenticate(user, password) do
    case Argon2.verify_pass(password, user.password_digest) do
      true -> SustainApiWeb.Guardian.encode_and_sign(user)
      _ -> {:error, :unauthorized}
    end
  end

  def get_current_resource(conn) do
    Guardian.Plug.current_resource(conn)
  end
end
