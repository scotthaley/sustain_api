# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :sustain_api,
  ecto_repos: [SustainApi.Repo]

# Configures the endpoint
config :sustain_api, SustainApiWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "xJcwWcsSLwMwJ/QWuCkyxnRVNdE+I5IBPGV/iDANCCXwR3ayk1KBw/Ct4n7ol+Qz",
  render_errors: [view: SustainApiWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: SustainApi.PubSub, adapter: Phoenix.PubSub.PG2]

config :sustain_api, SustainApiWeb.Guardian,
  issuer: "sustain_api",
  secret_key: "yOxIeXwgGYn3GTpDHzqjRN3LWqtFKqSZFr1kHPRH6RhtcUB53OEZYIhBG8Oen82e"

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
