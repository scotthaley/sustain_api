defmodule SustainApi.Repo.Migrations.PrepareUsersForAuthHandling do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :email, :string
      add :password_digest, :string
    end
  end
end
