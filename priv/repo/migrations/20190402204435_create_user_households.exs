defmodule SustainApi.Repo.Migrations.CreateUserHouseholds do
  use Ecto.Migration

  def change do
    create table(:user_households) do
      add :user_id, references(:users, on_delete: :delete_all)
      add :household_id, references(:households, on_delete: :delete_all)

      timestamps()
    end

    create index(:user_households, [:user_id])
    create index(:user_households, [:household_id])
  end
end
