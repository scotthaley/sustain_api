defmodule SustainApi.Repo.Migrations.AddItemNameUniqueConstraint do
  use Ecto.Migration

  def change do
    create unique_index(:items, [:user_id, :name], name: :item_name_unique)
  end
end
