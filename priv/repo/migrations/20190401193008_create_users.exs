defmodule SustainApi.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :current_household, :integer

      timestamps()
    end
  end
end
