defmodule SustainApi.Repo.Migrations.AddDefaultMeasurementToItems do
  use Ecto.Migration

  def change do
    alter table(:items) do
      add :default_measurement, :string
    end
  end
end
