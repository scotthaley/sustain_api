defmodule SustainApi.Repo.Migrations.ItemAddDescriptionField do
  use Ecto.Migration

  def change do
    alter table(:items) do
      add :description, :string
    end
  end
end
