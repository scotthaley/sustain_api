defmodule SustainApi.Repo.Migrations.AddUserIdToHousehold do
  use Ecto.Migration

  def change do
    alter table(:households) do
      add :user_id, references(:users, on_delete: :delete_all)
    end

    create index(:households, [:user_id])
  end
end
