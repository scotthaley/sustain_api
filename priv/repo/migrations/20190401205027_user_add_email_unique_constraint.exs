defmodule SustainApi.Repo.Migrations.UserAddEmailUniqueConstraint do
  use Ecto.Migration

  def change do
    create unique_index(:users, [:email], name: :email_unique)
  end
end
